package prospero_negocio;

import java.io.Serializable;
import javax.swing.ImageIcon;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

public class Espia implements Serializable{
	
	private ImageIcon foto;
	private double latitud; 
	private double longitud; 
	private String nombreClave;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Crea un espia tomando nombre, coordenada y ruta de imagen.
	 * @param String nombre 
	 * @param Coordinate coordenada 
	 * @param String rutaImagen 
	 */
	public Espia (String nombre, Coordinate coordenada, String rutaImagen ) 
	{
		verificarParametrosEspia(nombre);
		try
		{
			if(rutaImagen.equals(""))
				throw new NullPointerException();
			foto = new ImageIcon(this.getClass().getResource(rutaImagen));
		}
		catch (NullPointerException e)
		{
			foto = new ImageIcon(this.getClass().getResource("/image/mangosta_espia.png"));
		}
		latitud = coordenada.getLat();
		longitud = coordenada.getLon(); 
		nombreClave = nombre;
		
	}
	
	/**
	 * Verifica que el nombre del espia no este vacio.
	 * @param String nombre 
	 */
	private void verificarParametrosEspia(String nombre)
	{
		if (nombre.length()==0)
			throw new IllegalArgumentException("El nombre del espia no puede estar vacio");
	}
	
	//Para esta implementacion se decidio que si dos espias comparten nombres iguales, 
	//o ubicaciones cercanas, o imagenes iguales (distintas a la imagen por default), 
	//los espias sean considerados iguales para que la red de espionaje tenga coherencia.
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Espia)) {
			return false;
		}
		Espia other = (Espia) obj;
		
		return (comprobarCoordenadasIguales(other) 
		|| nombreClave.equals(other.nombreClave) 
		|| comprobarFotosIguales(other));
	}
	
	/**
	 * Devuelve true si el espia tiene la misma foto, false en caso contrario. 
	 * Si ambas imagenes son la imagen por default devuelve false.
	 * @param Espia agente
	 * @return boolean true o false
	 */
	private boolean comprobarFotosIguales(Espia agente)
	{
		ImageIcon estandar = new ImageIcon(this.getClass().getResource("/image/mangosta_espia.png"));
		if (agente.getFoto().getImage().equals(estandar.getImage()))
				return false;
		if (foto.getImage().equals(agente.getFoto().getImage()))
			return true;
		return false;
	}
	
	/**
	 * Devuelve true si el espia tomado como parametro esta en una 
	 * coordenada igual o muy cercana al otro espia, false en caso contrario.
	 * @param Espia agente
	 * @return boolean true o false
	 */
	private boolean comprobarCoordenadasIguales(Espia agente)
	{
		double lat = latitud;
		double lon = longitud;
		double centerLat = agente.getUbicacion().getLat();
		double centerLon = agente.getUbicacion().getLon();
        double distancia = Math.sqrt((((centerLat-lat)*(centerLat-lat)) + (centerLon-lon)*(centerLon-lon)));
        if (distancia < 0.001)
        	return true;
        return false;
	}
	
	/**
	 *  Devuelve la coordenada del espia en el mapa.
	 * @return Coordinate ubicacion del espia.
	 */
	public Coordinate getUbicacion()
	{
		return new Coordinate(latitud, longitud);
	}
	
	/**
	 *  Devuelve el MapMarkerDot del espia.
	 * @return MapMarkerDot del espia.
	 */
	public MapMarkerDot getMarker() 
	{
		return new MapMarkerDot (nombreClave, this.getUbicacion());
	}
	
	/**
	 *  Devuelve la foto del espia.
	 * @return ImageIcon foto del espia.
	 */
	public ImageIcon getFoto()
	{
		return foto;
	}
}