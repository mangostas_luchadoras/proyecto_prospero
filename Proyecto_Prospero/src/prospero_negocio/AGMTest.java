package prospero_negocio;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AGMTest {
	
	
	@Test
	public void agmSinVerticesTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
		@SuppressWarnings("unused")
		AGM agm = new AGM(0, new double [1][1]);
		});
	}
	
	@Test
	public void agmFilaNoCorrespondeTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
		@SuppressWarnings("unused")
		AGM agm = new AGM(5, new double [4][5]);
		});
	}
	
	@Test
	public void agmColumnaNoCorrespondeTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
		@SuppressWarnings("unused")
		AGM agm = new AGM(5, new double [5][4]);
		});
	}
	
	@Test
	public void agmColumnaYFilaNoCorrespondeTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
		@SuppressWarnings("unused")
		AGM agm = new AGM(5, new double [2][4]);
		});
	}
	
	@Test
	public void agmCantidadVerticesColumnaYFilaNoCorrespondeTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
		@SuppressWarnings("unused")
		AGM agm = new AGM(0, new double [0][0]);
		});
	}
	
	@Test
	public void agmUnVerticeTest() 
	{
		AGM agm = new AGM(1, new double [1][1]);
		assertTrue(agm.getAristaMayor() == 0.0);
		assertTrue(agm.getAristaMenor() == 0.0);
		assertTrue(agm.getPesoAGM() == 0.0);
		assertTrue(agm.getPromedioAristas() == 0.0);
		assertEquals(agm.getAGM(),  new ArrayList<Arista>());
	}

	@Test
	public void agmCorrectoTest() 
	{
		AGM agm = new AGM(9, matrizDistanciasDiapositivaProgra3());
		assertTrue(agm.getAristaMayor() == 9.0);
		assertTrue(agm.getAristaMenor() == 1.0);
		assertTrue(agm.getPesoAGM() == 38.0);
		assertTrue(agm.getPromedioAristas() == 4.75);
		assertEquals(agm.getAGM(), aristasEsperadas());
	}
	
	private double [][] matrizDistanciasDiapositivaProgra3()
	{
		double [][] distancias = new double [9][9];
	
		distancias[0][1] = 4.0;  distancias[1][0] = 4.0;
		distancias[0][7] = 8.0;  distancias[7][0] = 8.0;	
		distancias[1][2] = 8.0;  distancias[2][1] = 8.0;
		distancias[1][7] = 12.0; distancias[7][1] = 12.0;
		distancias[2][3] = 6.0;  distancias[3][2] = 6.0;
		distancias[2][5] = 4.0;  distancias[5][2] = 4.0;
		distancias[2][8] = 3.0;  distancias[8][2] = 3.0;	
		distancias[3][4] = 9.0;  distancias[4][3] = 9.0;
		distancias[3][5] = 13.0; distancias[5][3] = 13.0;
		distancias[4][5] = 10.0; distancias[5][4] = 10.0;
		distancias[5][6] = 3.0;  distancias[6][5] = 3.0;
		distancias[6][7] = 1.0;  distancias[7][6] = 1.0;
		distancias[6][8] = 5.0;  distancias[8][6] = 5.0;
		distancias[7][8] = 6.0;  distancias[8][7] = 6.0;
		
		llenarInfinitosMatrizDistancias(distancias);
		
		return distancias;
	}
	
	private void llenarInfinitosMatrizDistancias(double [][] distancias)
	{
		for (int f = 0; f < distancias.length; f++)
			for (int c = 0; c < distancias[f].length; c++)
				if (f != c && distancias[f][c] == 0.0) 
				{
					distancias[f][c] = Double.POSITIVE_INFINITY;
				}
	}
	
	private ArrayList<Arista> aristasEsperadas()
	{
		ArrayList<Arista> aristas = new ArrayList<Arista>();
		aristas.add(new Arista(0, 1));
		aristas.add(new Arista(0, 7));
		aristas.add(new Arista(7, 6));
		aristas.add(new Arista(6, 5));  
		aristas.add(new Arista(5, 2));
		aristas.add(new Arista(2, 8));
		aristas.add(new Arista(2, 3));
		aristas.add(new Arista(3, 4));
		
		return aristas;
	}

}
