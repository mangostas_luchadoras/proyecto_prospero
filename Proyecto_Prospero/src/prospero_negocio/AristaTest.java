package prospero_negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AristaTest {
	
	@Test
	public void aristaPrimerValorNegativoTest()
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
   	    @SuppressWarnings("unused")
		Arista a = new Arista(-1,0);;
		});
	}

	@Test
	public void aristaSegundoValorNegativoTest()
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
	   	    @SuppressWarnings("unused")
	   	    Arista a = new Arista(1,-1);
		});
	}
	
	@Test
	public void aristaAmbosValorNegativoTest()
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Arista a = new Arista(-1,-1);
		});
	}

	@Test
	public void aristaCorrectaTest()
	{
		Arista a = new Arista(4,3);
		assertTrue(a.getVerticeU()==4);
		assertTrue(a.getVerticeV()==3);
	}
	
	@Test
	public void compararAristasDistintasTest()
	{
		Arista a = new Arista(4,3);
		Arista b = new Arista(3,4);
		assertFalse(a.equals(b));
	}
	
	@Test
	public void compararAristasIgualesTest()
	{
		Arista a = new Arista(4,3);
		Arista b = new Arista(4,3);
		assertTrue(a.equals(b));
	}
}
