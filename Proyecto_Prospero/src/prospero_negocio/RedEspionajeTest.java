package prospero_negocio;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

class RedEspionajeTest {
	
	@Test
	public void redEspiaVaciaTest() 
	{
		RedEspionaje red = new RedEspionaje();
		assertTrue(red.tamanioRedEspias() == 0);
		assertTrue(red.getMatrizDistancias() == null && red.getAGM() == null);
	}
	
	@Test
	public void agregarEspiaRepetidoRedEspiaVaciaTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			RedEspionaje red= new RedEspionaje();
			agregarCincoEspias(red);
			red.agregarEspia(new Espia("carlinA", new Coordinate (-34.522222, -58.7),""));
			});
	}
	
	@Test
	public void AgregarEspiaTest() 
	{
		RedEspionaje red = new RedEspionaje();
		red.agregarEspia(new Espia("carlinA", new Coordinate (-34.522222, -58.7),""));
		assertTrue(red.tamanioRedEspias() == 1);
		assertTrue(red.getMatrizDistancias()[0][0] == 0.0);
		assertTrue(red.getAGM().equals(new AGM(red.tamanioRedEspias(), red.getMatrizDistancias())));
	}
	
	@Test
	public void AgregarCincoEspiasTest() 
	{
		RedEspionaje red= new RedEspionaje();
		agregarCincoEspias(red);
		assertTrue(red.tamanioRedEspias() == 5);
		assertTrue(cincoEspiasDisctancias(red.getMatrizDistancias()));
		assertTrue(red.getAGM().equals(AGMdeCincoEspias()));
	}
	
	@Test
	public void eliminarEspiaRedEspiaVaciaTest() 
	{
		Assertions.assertThrows(NullPointerException.class, () -> {
			RedEspionaje red= new RedEspionaje();
			red.eliminarEspia(new Coordinate (-34.522222, -58.7));
			});
	}
	
	@Test
	public void eliminarUnEspiaRedEspiaTest() 
	{
		RedEspionaje red= new RedEspionaje();
		red.agregarEspia(new Espia("carlinA", new Coordinate (-34.522222, -58.7),""));
		red.eliminarEspia(new Coordinate (-34.522222, -58.7));
		assertTrue(red.tamanioRedEspias() == 0);
		assertTrue(red.getMatrizDistancias() == null && red.getAGM() == null);	
	}
	
	@Test
	public void eliminarUnEspiaRedSeisEspiasTest() 
	{
		RedEspionaje red= new RedEspionaje();
		agregarCincoEspias(red);
		red.agregarEspia(new Espia("carlin", new Coordinate (-35.522222, -58.7),""));
		red.eliminarEspia(new Coordinate (-35.522222, -58.7));
		assertTrue(red.tamanioRedEspias() == 5);
		assertTrue(cincoEspiasDisctancias(red.getMatrizDistancias()));
		assertTrue(red.getAGM().equals(AGMdeCincoEspias()));
	}
	
	@Test
	public void eliminarUnEspiaConCoordenadaCercanaRedSeisEspiasTest() 
	{
		RedEspionaje red= new RedEspionaje();
		agregarCincoEspias(red);
		red.agregarEspia(new Espia("carlin", new Coordinate (-35.523456, -58.723456),""));
		red.eliminarEspia(new Coordinate (-35.523457, -58.723455));
		assertTrue(red.tamanioRedEspias() == 5);
		assertTrue(cincoEspiasDisctancias(red.getMatrizDistancias()));
		assertTrue(red.getAGM().equals(AGMdeCincoEspias()));
	}
	
	@Test
	public void eliminarUnEspiaConCoordenadaIncorrectaRedCincoEspiasTest() 
	{
		Assertions.assertThrows(NullPointerException.class, () -> {
		RedEspionaje red= new RedEspionaje();
		agregarCincoEspias(red);
		red.eliminarEspia(new Coordinate (-20.5, 48.7));
		});
	}
	
	private boolean cincoEspiasDisctancias(double [][] distancias)
	{
		boolean ret = true;
		ret = ret && diagonalesCincoEspiasDisctancias(distancias);
		ret = ret && filaCeroCincoEspiasDisctancias(distancias);
		ret = ret && filaUnoCincoEspiasDisctancias(distancias); 
		ret = ret && filaDosCincoEspiasDisctancias(distancias); 
		ret = ret && filaTresCincoEspiasDisctancias(distancias); 
		ret = ret && filaCuatroCincoEspiasDisctancias(distancias); 
		
		return ret;
	}	
	
	private boolean diagonalesCincoEspiasDisctancias(double [][] distancias) 
	{
		boolean ret = true;
		ret = ret && distancias[0][0] == 0.0;
		ret = ret && distancias[1][1] == 0.0;
		ret = ret && distancias[2][2] == 0.0;
		ret = ret && distancias[3][3] == 0.0;
		ret = ret && distancias[4][4] == 0.0;
		
		return ret;
	}
 
	private boolean filaCeroCincoEspiasDisctancias(double [][] distancias) 
	{
		boolean ret = true;
		ret = ret && distancias[0][1] == 4.188476077058086;
		ret = ret && distancias[0][2] == 10.191043940786109; 
		ret = ret && distancias[0][3] == 54.79467993848688;
		ret = ret && distancias[0][4] == 22.112057797699713;
		
		return ret;
	}
	
	private boolean filaUnoCincoEspiasDisctancias(double [][] distancias) 
	{
		boolean ret = true;
		ret = ret && distancias[1][0] == 4.188476077058086;
		ret = ret && distancias[1][2] == 14.330807336978808;
		ret = ret && distancias[1][3] == 55.104655285084625;
		ret = ret && distancias[1][4] == 22.18963947563536;
		
		return ret;
	}
	
	private boolean filaDosCincoEspiasDisctancias(double [][] distancias) 
	{
		boolean ret = true;
		ret = ret && distancias[2][0] == 10.191043940786109;
		ret = ret && distancias[2][1] == 14.330807336978808;
		ret = ret && distancias[2][3] == 53.53102270392545 ;
		ret = ret && distancias[2][4] == 26.600162185617823;
		
		return ret;
	}	
	
	private boolean filaTresCincoEspiasDisctancias(double [][] distancias) 
	{
		boolean ret = true;
		ret = ret && distancias[3][0] == 54.79467993848688;
		ret = ret && distancias[3][1] == 55.104655285084625;
		ret = ret && distancias[3][2] == 53.53102270392545;
		ret = ret && distancias[3][4] == 76.89399348880875;
		
		return ret;
	}
	
	private boolean filaCuatroCincoEspiasDisctancias(double [][] distancias) 
	{
		boolean ret = true;
		ret = ret && distancias[4][0] == 22.112057797699713;
		ret = ret && distancias[4][1] == 22.18963947563536;
		ret = ret && distancias[4][2] == 26.600162185617823;
		ret = ret && distancias[4][3] == 76.89399348880875;
		
		return ret;
	}
	
	private AGM AGMdeCincoEspias() 
	{
		RedEspionaje red = new RedEspionaje();
		agregarCincoEspias(red);
		AGM agm = new AGM(red.tamanioRedEspias(), red.getMatrizDistancias());
		
		return agm;
	}
	
	private void agregarCincoEspias(RedEspionaje red)
	{
		red.agregarEspia(new Espia("carlinA", new Coordinate (-34.522222, -58.7),""));
		red.agregarEspia(new Espia("carlinB", new Coordinate (-34.520388, -58.745564),""));
		red.agregarEspia(new Espia("carlinC", new Coordinate (-34.51, -58.59),""));
		red.agregarEspia(new Espia("carlinD", new Coordinate (-34.03, -58.65),""));
		red.agregarEspia(new Espia("carlinE", new Coordinate (-34.72, -58.73),""));
	}
	
}
