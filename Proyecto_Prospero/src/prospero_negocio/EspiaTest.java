package prospero_negocio;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

class EspiaTest {

	@Test
	public void EspiaSinNombreTest() 
	{
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
		@SuppressWarnings("unused")
		Espia agente = new Espia("", new Coordinate (-34.522222, -58.7), "");
		});
	}
	
	@Test
	public void compararEspiasNombresDistintosMismaCoordenadaTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7),"");
		Espia agente2 = new Espia("Smart", new Coordinate (-34.522222, -58.7),"");
		
		assertTrue(agente1.equals(agente2));
	}
	
	@Test
	public void compararEspiasCoordenadasDistintasMismoNombreTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7),"");
		Espia agente2 = new Espia("Carlin", new Coordinate (-34.520388, -58.745564),"");
		
		assertTrue(agente1.equals(agente2));
	}
	
	@Test
	public void compararEspiasCoordenadasDistintasNombresDistintosImagenesCargadasDistintasTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7), "/image/mangosta_icon.jpeg");
		Espia agente2 = new Espia("Smart", new Coordinate (-34.520388, -58.745564), "/image/mangosta_presentacion.png");
		
		assertFalse(agente1.equals(agente2));
	}
	
	@Test
	public void compararEspiasCoordenadasDistintasNombresDistintosUnaImagenCargadaDistintaTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7), "/image/mangosta_icon.jpeg");
		Espia agente2 = new Espia("Smart", new Coordinate (-34.520388, -58.745564),"");
		
		assertFalse(agente1.equals(agente2));
	}
	
	@Test
	public void compararEspiasTodosParametrosIgualesTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7),"");
		Espia agente2 = new Espia("Carlin", new Coordinate (-34.522222, -58.7),"");
		
		assertEquals(agente1, agente2);
	}
	
	@Test
	public void compararEspiasAliasingTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7),"");
		Espia agente2 = agente1;
		
		assertEquals(agente1, agente2);
	}
	
	@Test
	public void compararEspiasConImagenesAgregadasIgualesTest() 
	{
		Espia agente1 = new Espia("Carlin", new Coordinate (-34.522222, -58.7), "/image/mangosta_icon.jpeg");
		Espia agente2 = new Espia("Carlin", new Coordinate (-34.522222, -58.7), "/image/mangosta_icon.jpeg");
		
		assertEquals(agente1, agente2);
	}
	
	
	
}
