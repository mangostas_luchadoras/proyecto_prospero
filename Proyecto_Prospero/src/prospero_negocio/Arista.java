package prospero_negocio;

import java.io.Serializable;

public class Arista implements Serializable {
	
	private Integer verticeU;
	private Integer verticeV;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Construye una Arista con los vertices U y V.
	 * @param verticeU entero
	 * @param verticeV entero
	 */
	public Arista(Integer verticeU, Integer verticeV)
	{
		verificarParametroNegativoEnArista(verticeU,verticeV);
		this.verticeU = verticeU;
		this.verticeV = verticeV;
	}
	
	/**
	 * Verifica que los enteros verticeU y verticeV no sean negativos.
	 * @param verticeU entero
	 * @param verticeV entero
	 */
	private void verificarParametroNegativoEnArista(Integer verticeU, Integer verticeV)
	{
		if (verticeU < 0 || verticeV < 0)
			throw new IllegalArgumentException("El valor del vertice no es valido: Utilizar enteros positivos");
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Arista)) {
			return false;
		}
		Arista other = (Arista) obj;
		return (verticeU == other.verticeU && verticeV== other.verticeV);
	}

	/**
	 * Devuelve vertice U.
	 * @return vertice U
	 */
	public Integer getVerticeU()
	{
		return verticeU;
	}
	
	/**
	 * Devuelve vertice V.
	 * @return vertice V
	 */
	public Integer getVerticeV()
	{
		return verticeV;
	}

}
