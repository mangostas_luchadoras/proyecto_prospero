package prospero_negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class AGM implements Iterable<Arista>, Serializable {
	
	private ArrayList<Arista> agm;
	private double aristaMenor;
	private double aristaMayor;
	private double pesoAGM;
	private double promedioAristas;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Construye un Arbol Generador Minimo.
	 * @param entero cantidadVertices 
	 * @param matrizDeDistancias double [][]
	 */
	public AGM(int cantidadVertices, double [][] matrizDeDistancias)
	{
		verificarParametrosAGM(cantidadVertices, matrizDeDistancias);
		agm = prim(cantidadVertices, matrizDeDistancias);
	}
	
	/**
	 * Realiza el algoritmo de Prim para obtener un AGM.
	 * @param cantidadVertices entero 
	 * @param matrizDeDistancias double [][]
	 * @return ArrayList aristas del AGM
	 */
	private ArrayList<Arista> prim(int cantidadVertices, double [][] matrizDeDistancias)
	{
		ArrayList<Integer> verticesVisitados = new ArrayList<Integer>();
		verticesVisitados.add(0);
		ArrayList<Arista> aristas = new ArrayList<Arista>();
		
		while (verticesVisitados.size()!=cantidadVertices)
		{
			Arista aux = aristaMenor(verticesVisitados, matrizDeDistancias);
			aristas.add(aux);
			verticesVisitados.add(aux.getVerticeV());
		}
		calcularEstadisticasAGM(aristas, matrizDeDistancias);
		
		return aristas;
	}
	
	/**
	 * Devuelve la arista de menor peso partiendo desde los vertices visitados hacia los vertices 
	 * no visitados descartando las aristas generadas por dos vertices ya visitados.
	 * @param verticesVisitados entero  
	 * @param matrizDeDistancias double [][] 
	 * @return Arista arista de menor peso desde un vertice visitado a uno no visitado
	 */
	private Arista aristaMenor(ArrayList<Integer> verticesVisitados, double [][] matrizDeDistancias)
	{
		Integer partida = -1;
		Integer visitado = -1;
		double menor = Double.POSITIVE_INFINITY;
		for (Integer e: verticesVisitados)
			for (int j=0; j<matrizDeDistancias[0].length; j++) 
				if (!verticesVisitados.contains(j) && matrizDeDistancias[e][j] < menor)
				{
					partida = e;
					visitado = j;
					menor = matrizDeDistancias[e][j];
				}
		return new Arista(partida,visitado);
	}
	
	/**
	 * Calcula la arista de menor peso, la de mayor y el peso total del AGM.
	 * @param ArrayList aristas del AGM
	 * @param matrizDeDistancias double [][]
	 */
	private void calcularEstadisticasAGM(ArrayList<Arista> aristas, double [][] matrizDeDistancias) 
	{
		aristaMenor = Double.POSITIVE_INFINITY;
		aristaMayor = Double.NEGATIVE_INFINITY;
		pesoAGM = 0.0;
		
		
		if (aristas.size() == 0)
		{
			agm = new ArrayList<Arista>();
			aristaMenor = 0.0;
			aristaMayor = 0.0;
			promedioAristas = 0.0;
		}
		else
		{
			for (Arista a: aristas)
			{
				if (matrizDeDistancias[a.getVerticeU()][a.getVerticeV()] < aristaMenor)
					aristaMenor = matrizDeDistancias[a.getVerticeU()][a.getVerticeV()];
				if (matrizDeDistancias[a.getVerticeU()][a.getVerticeV()] > aristaMayor)
					aristaMayor = matrizDeDistancias[a.getVerticeU()][a.getVerticeV()];
				pesoAGM+= matrizDeDistancias[a.getVerticeU()][a.getVerticeV()];
			}
			promedioAristas = pesoAGM/aristas.size();
		}
	}
	
	/**
	 * Verifica que la cantidad de vertices sea entera y  
	 * la matriz de distancias tenga un tamanio correspondiente.
	 * @param cantidadVertices entero 
	 * @param matrizDeDistancias double [][]
	 */
	private void verificarParametrosAGM(int cantidadVertices, double [][] matrizDeDistancias)
	{
		if (cantidadVertices < 1)
			throw new IllegalArgumentException("La cantidad de vertices no es valida: Utilizar enteros positivos");
		
		if (matrizDeDistancias.length != cantidadVertices || matrizDeDistancias[0].length != cantidadVertices)
			throw new IllegalArgumentException("La cantidad de vertices no corresponde con la matriz de distancias");
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AGM)) {
			return false;
		}
		AGM other = (AGM) obj;
		return (agm.equals(other.agm) && aristaMenor == other.aristaMenor && aristaMayor == other.aristaMayor && pesoAGM == other.pesoAGM);
	}
	
	@Override
	public Iterator<Arista> iterator() {
		
		return agm.iterator();
	}
	
	/**
	 * Devuelve el peso de la arista menor del AGM.
	 * @return double arista menor
	 */
	public double getAristaMenor()
	{
		return aristaMenor;
	}
	
	/**
	 * Devuelve el peso de la arista mayor del AGM.
	 * @return double arista mayor
	 */
	public double getAristaMayor()
	{
		return aristaMayor;
	}
	
	/**
	 * Devuelve el peso total del AGM.
	 * @return double peso AGM
	 */
	public double getPesoAGM()
	{
		return pesoAGM;
	}
	
	/**
	 * Devuelve el peso total del AGM.
	 * @return double peso AGM
	 */
	public double getPromedioAristas()
	{
		return promedioAristas;
	}
	
	/**
	 * Devuelve una lista con aristas del AGM.
	 * @return ArrayList aristas del AGM
	 */
	public ArrayList<Arista> getAGM()
	{
		return agm;
	}

}
