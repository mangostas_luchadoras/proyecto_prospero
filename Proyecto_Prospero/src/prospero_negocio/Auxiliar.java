package prospero_negocio;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Auxiliar {
	
	/**
	 * Calcula la distancia entre dos puntos 
	 * del mapa terrestre usando el algoritmo de Vincenty y 
	 * devuelve el resultado en kilometros.
	 * @param  x double coordenada latitud
	 * @param  y double coordenada longitud
	 * @return double distancia entre x e y en kilometros.
	 */
	public static double calcularDistanciaVincenty( Coordinate x, Coordinate y) 
	{
		double latX= Math.toRadians(x.getLat());
		double latY= Math.toRadians(y.getLat());
		double lonX= Math.toRadians(x.getLon());
		double lonY= Math.toRadians(y.getLon());
		double a = 6378.137;
		double b = 6356.7523142;
		double f = 1 / 298.257223563;
		double L = lonY- lonX;
		double tanU1 = (1-f) * Math.tan(latX), cosU1 = 1 / Math.sqrt((1 + tanU1*tanU1)), sinU1 = tanU1 * cosU1;
		double tanU2 = (1-f) * Math.tan(latY), cosU2 = 1 / Math.sqrt((1 + tanU2*tanU2)), sinU2 = tanU2 * cosU2;
		double λ = L, λʹ, iterationLimit = 100;
		double sinλ;
		double cosλ; 
		double cosSqα;
		double σ;
		double sinσ;
		double cos2σM;
		double cosσ;
		
		do {
			sinλ = Math.sin(λ);
			cosλ = Math.cos(λ);
			double sinSqσ = (cosU2*sinλ) * (cosU2*sinλ) + (cosU1*sinU2-sinU1*cosU2*cosλ) * (cosU1*sinU2-sinU1*cosU2*cosλ);
			sinσ = Math.sqrt(sinSqσ);
		    if (sinσ==0) return 0;  // puntos coincidentes
		    cosσ = sinU1*sinU2 + cosU1*cosU2*cosλ;
		    σ = Math.atan2(sinσ, cosσ);
		    double sinα = cosU1 * cosU2 * sinλ / sinσ;
		    cosSqα = 1 - sinα*sinα;
		    if (cosSqα == 0)    // linea equatorial: cosSqα=0 (§6)
		    	cos2σM = 0;
		    else
		    	cos2σM = cosσ - 2*sinU1*sinU2/cosSqα; 
		    double C = f/16*cosSqα*(4+f*(4-3*cosSqα));
		    λʹ = λ;
		    λ = L + (1-C) * f * sinα * (σ + C*sinσ*(cos2σM+C*cosσ*(-1+2*cos2σM*cos2σM)));
		} while (Math.abs(λ-λʹ) > 1e-12 && --iterationLimit>0);
		
		if (iterationLimit==0) 
			return Math.PI * 6371;
		
		double uSq = cosSqα * (a*a - b*b) / (b*b);
		double A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
		double B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));
		double Δσ = B*sinσ*(cos2σM+B/4*(cosσ*(-1+2*cos2σM*cos2σM)-B/6*cos2σM*(-3+4*sinσ*sinσ)*(-3+4*cos2σM*cos2σM)));
		
		return b*A*(σ-Δσ);
	}

}
