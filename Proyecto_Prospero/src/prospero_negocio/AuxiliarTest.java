package prospero_negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

class AuxiliarTest {

	
	@Test
	public void DistanciaCeroACeroTest() 
	{
		Coordinate x = new Coordinate (0.0, 0.0);
		Coordinate y = new Coordinate (0.0, 0.0);
		
		assertTrue(Auxiliar.calcularDistanciaVincenty(x, y) == 0.0);
	}

	@Test
	public void DistanciaCeroAUngsTest() 
	{
		Coordinate x = new Coordinate (0.0, 0.0);
		Coordinate y = new Coordinate (-34.522222, -58.7);
		
		assertTrue(Auxiliar.calcularDistanciaVincenty(x, y) == 7188.000708335194);
	}
	
	@Test
	public void DistanciaUnpazACeroTest() 
	{
		Coordinate x = new Coordinate (-34.520388, -58.745564);
		Coordinate y = new Coordinate (0.0, 0.0);
		assertTrue(Auxiliar.calcularDistanciaVincenty(x, y) == 7191.894431598869);
	}
	
	@Test
	public void DistanciaUngsAUnpazTest() 
	{
		Coordinate x = new Coordinate (-34.522222, -58.7);
		Coordinate y = new Coordinate (-34.520388, -58.745564);
		
		assertTrue(Auxiliar.calcularDistanciaVincenty(x, y) == 4.188476077058085);
	}
	
	@Test
	public void DistanciaAntipodasTest() {
		Coordinate x = new Coordinate (-34.522222, -58.7);
		Coordinate y = new Coordinate (34.522222, 121.3);
		
		assertTrue(Auxiliar.calcularDistanciaVincenty(x, y) == Math.PI * 6371);
	}
}
