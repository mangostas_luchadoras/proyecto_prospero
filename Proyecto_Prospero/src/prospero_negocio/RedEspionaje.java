package prospero_negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class RedEspionaje implements Iterable<Espia>, Serializable{
	
	private ArrayList<Espia> listaEspias;
	private double [][] distancias;
	private AGM agm;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Construye una Red de Espionaje que contiene una lista de espias 
	 * una matriz de distancias y un AGM.
	 */
	public RedEspionaje() 
	{
		listaEspias = new ArrayList<Espia>();
		distancias = null;
		agm = null;
	}
	
	/**
	 * Llena la matriz de distancias de la Red de Espionaje
	 * calculando la distancia entre los agentes de la lista de espias.
	 */
	private void llenarMatrizDistancias()
	{
		distancias = new double [listaEspias.size()][listaEspias.size()];
		for (int f = 0; f < distancias.length; f++)
			for (int c = 0; c < distancias[f].length; c++)
				if (f != c) 
				{
					distancias[f][c] = Auxiliar.calcularDistanciaVincenty(listaEspias.get(f).getUbicacion(), listaEspias.get(c).getUbicacion());
					distancias[c][f] = distancias[f][c];
				}
				else
				{
					distancias[c][f] = 0.0;
				}
	}
	
	/**
	 * Agrega un espia a la Red de Espionaje.
	 * @param Espia agente
	 */
	public void agregarEspia(Espia agente) throws IllegalArgumentException
	{
		VerificarEspiaNuevo(agente);
		listaEspias.add(agente);
		llenarMatrizDistancias();
		agm = new AGM(listaEspias.size(), distancias);
	}
	
	/**
	 * Elimina un espia de la Red de Espionaje utilizando una coordenada.
	 * @param Coordinate coordenada
	 * @throws NullPointerException
	 */
	public void eliminarEspia(Coordinate coordenada) throws NullPointerException
	{
		listaEspias.remove(getEspiaDesdeCoordenada(coordenada));
		if (listaEspias.size() == 0)
		{
			distancias = null;
			agm = null;
		}
		else
		{
			llenarMatrizDistancias();
			agm = new AGM(listaEspias.size(), distancias);
		}
	}
	
	/**
	 * Devuelve un espia de la Red de Espionaje utilizando una coordenada.
	 * @param Coordinate coordenada
	 * @return Espia agente
	 * @throws NullPointerException
	 */
	public Espia getEspiaDesdeCoordenada(Coordinate coordenada) throws NullPointerException
	{
		double lat = coordenada.getLat();
		double lon = coordenada.getLon();
		for (Espia agente : listaEspias) 
		{
			double centerLat = agente.getUbicacion().getLat();
			double centerLon = agente.getUbicacion().getLon();
            double distancia = Math.sqrt((((centerLat-lat)*(centerLat-lat)) + (centerLon-lon)*(centerLon-lon)));
            if (distancia < 0.001)
            	return agente;
		}
		throw new NullPointerException("La coordenada no corresponde con un espia.");
	}
	
	/**
	 * Verifica que el espia no este en la Red de Espionaje.
	 * @param Espia
	 */
	private void VerificarEspiaNuevo(Espia agente)
	{
		if(listaEspias.contains(agente))
			throw new IllegalArgumentException("El espía que se quiere agregar ya existe");
	}
	
	/**
	 * Devuelve un espia de la lista de espías de la Red de Espionaje utilizando un indice.
	 * @param indice entero no negativo
	 * @return Espia
	 */
	public Espia getEspia(int indice)
	{
		return listaEspias.get(indice);
	}
	
	/**
	 * Devuelve una lista con aristas del AGM de la Red de Espionaje.
	 * @return ArrayList aristas del AGM
	 */
	public AGM getAGM()
	{
		return agm;
	}
	
	/**
	 * Devuelve la matriz de distancias de la Red de Espionaje.
	 * @return double [][] matriz de distancias
	 */
	public double [][] getMatrizDistancias()
	{
		return distancias;
	}
	
	/**
	 * Devuelve la cantidad de espias de la Red de Espionaje.
	 * @return
	 */
	public int tamanioRedEspias()
	{
		return listaEspias.size();
	}

	@Override
	public Iterator<Espia> iterator() {
		
		return listaEspias.iterator();
	}
}
