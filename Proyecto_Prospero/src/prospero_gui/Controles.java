package prospero_gui;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import prospero_negocio.Espia;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.Font;
import javax.swing.ButtonGroup;

public class Controles extends JPanel 
{
	private JButton btnAgregarEspia;
	private JButton btnEliminarEspia;
	private JButton btnMostrarRedCom;
	private JButton btnGuardar;
	
	private static final long serialVersionUID = 1L;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	/**
	 * Crea un panel con los botones que controlan la aplicacion.
	 * @param CentroOperaciones hq
	 */
	public Controles(CentroOperaciones hq) 
	{
		setBorder(null);
		setLayout(new GridLayout(0, 1, 0, 0));
		
		crearBotonAgregarEspia(hq);
		crearBotonEliminararEspia(hq);
		crearBotonMostarRedComunicacion(hq);
		crearBotonGuardar(hq);
	}
	
	/**
	 * Crea un boton para agregar un espia en la aplicacion.
	 * @param CentroOperaciones hq
	 */
	private void crearBotonAgregarEspia(CentroOperaciones hq) 
	{
		btnAgregarEspia = new JButton("Agregar Espia");
		btnAgregarEspia.setPressedIcon(new ImageIcon(Controles.class.getResource("/image/boton_rojo.png")));
		acomodarBoton(btnAgregarEspia);
		btnAgregarEspia.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				btnAgregarEspia.setSelected(true);
				btnEliminarEspia.setSelected(false);
				hq.getMapa().addMouseListener(new MouseAdapter() 
				{
					@Override
					public void mouseClicked(MouseEvent f) 
					{
						if (f.getButton() == MouseEvent.BUTTON1 && btnAgregarEspia.isSelected())
						{
						    Coordinate posicion = (Coordinate)hq.getMapa().getPosition(f.getPoint());
							String nombre = JOptionPane.showInputDialog("Nombre: ");
							String foto = JOptionPane.showInputDialog("Ingrese la ruta de la foto.");
						    try 
						    {
						    	hq.getAgentes().agregarEspia(new Espia(nombre, posicion, foto));	
						    }
							catch (IllegalArgumentException e)
						    {
								JOptionPane.showMessageDialog(getParent(), e.getMessage());
						    }
						    hq.crearRedComunicacion();
						    hq.getMapa().removeAllMapMarkers();
						    hq.mostrarEspiasEnMapa();
						    hq.mostrarRedComunicacion(btnMostrarRedCom.isSelected());
						    btnAgregarEspia.setSelected(false);
						}
					}
				});
			}
		});
		buttonGroup.add(btnAgregarEspia);
		add(btnAgregarEspia);
	}
	
	/**
	 * Crea un boton para eliminar un espia en la aplicacion.
	 * @param CentroOperaciones hq
	 */
	private void crearBotonEliminararEspia(CentroOperaciones hq) 
	{
		btnEliminarEspia = new JButton("Eliminar Espia");
		acomodarBoton(btnEliminarEspia);
		btnEliminarEspia.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				btnEliminarEspia.setSelected(true);
				btnAgregarEspia.setSelected(false);
				hq.getMapa().addMouseListener(new MouseAdapter() 
				{
					@Override
					public void mouseClicked(MouseEvent f) 
					{
						if (f.getButton() == MouseEvent.BUTTON1 && btnEliminarEspia.isSelected())
						{
						    Coordinate posicion = (Coordinate)hq.getMapa().getPosition(f.getPoint());
							try
							{
								hq.getAgentes().eliminarEspia(posicion);
							}
						    catch (NullPointerException e)
						    {
						    	JOptionPane.showMessageDialog(getParent(), e.getMessage());
						    }
						    hq.crearRedComunicacion();
						    hq.mostrarEspiasEnMapa();
						    hq.mostrarRedComunicacion(btnMostrarRedCom.isSelected());
						    btnEliminarEspia.setSelected(false);
						}
					}
				});
			}
		});
		buttonGroup.add(btnEliminarEspia);
		add(btnEliminarEspia);
	}
	
	/**
	 * Crea un boton para mostrar la red de comunicaiones en la aplicacion.
	 * @param CentroOperaciones hq
	 */
	private void crearBotonMostarRedComunicacion(CentroOperaciones hq) 
	{
		btnMostrarRedCom = new JButton("Red Comunicacion");
		acomodarBoton(btnMostrarRedCom);
		btnMostrarRedCom.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				btnMostrarRedCom.setSelected(!btnMostrarRedCom.isSelected());
				hq.mostrarRedComunicacion(btnMostrarRedCom.isSelected());
			}
		});
		add(btnMostrarRedCom);
	}
	
	/**
	 * Crea un boton para guardar la red de espionaje actual en la aplicacion.
	 * @param CentroOperaciones hq
	 */
	private void crearBotonGuardar(CentroOperaciones hq) 
	{
		btnGuardar = new JButton("Guardar");
		acomodarBoton(btnGuardar);
		btnGuardar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				hq.guardarRedEspionaje();
			}
		});
		add(btnGuardar);
	}
	
	/**
	 * Configura el formato de los los botones.
	 * @param JButton boton que sera configurado
	 */
	private void acomodarBoton(JButton boton)
	{
		boton.setBorder(null);
		boton.setBorderPainted(false);
		boton.setContentAreaFilled(false);
		boton.setAlignmentX(Component.CENTER_ALIGNMENT);
		boton.setForeground(Color.WHITE);
		boton.setHorizontalTextPosition(SwingConstants.CENTER);
		boton.setFont(new Font("OCR A Extended", Font.BOLD, 14));
		boton.setIcon(new ImageIcon(Controles.class.getResource("/image/boton_negro.png")));
		boton.setSelectedIcon(new ImageIcon(Controles.class.getResource("/image/boton_rojo.png")));
		boton.setPressedIcon(new ImageIcon(Controles.class.getResource("/image/boton_rojo.png")));
	}
	
	/**
	 * Devuelve un boolean que indica si se esta eliminando un espia.
	 * @return boolean
	 */
	public boolean eliminandoAgente()
	{
		return btnEliminarEspia.isSelected();
	}
}
