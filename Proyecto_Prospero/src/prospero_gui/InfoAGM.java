package prospero_gui;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import prospero_negocio.AGM;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Font;

public class InfoAGM extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private JTextPane txtpnAristaMenor;
	private JTextPane txtpnAristaMayor;
	private JTextPane txtpnPesoAgm;
	private JTextPane txtpnValorAristaMenor;
	private JTextPane txtpnValorAristaMayor;
	private JTextPane txtpnValorPesoAGM;
	private JTextPane txtpnPesoPromedioAristas;
	private JTextPane txtpnValorPromedioAristas;

	/**
	 * Crea el panel donde se muestra la informacion del AGM.
	 */
	public InfoAGM() 
	{
		setBackground(Color.BLACK);
		setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setLayout(new GridLayout(0, 4, 3, 3));
		
		setVariables();
	}

	/**
	 * Setea el formato de los paneles de texto.
	 */
	private void setVariables() 
	{				
		txtpnAristaMenor = new JTextPane();
		configurarTxtPane(txtpnAristaMenor, "Peso Arista Menor");
		add(txtpnAristaMenor);
		
		txtpnAristaMayor = new JTextPane();
		configurarTxtPane(txtpnAristaMayor, "Peso Arista Mayor");
		add(txtpnAristaMayor);
		
		txtpnPesoPromedioAristas = new JTextPane();
		configurarTxtPane(txtpnPesoPromedioAristas, "Peso Promedio Aristas");
		add(txtpnPesoPromedioAristas);
		
		txtpnPesoAgm = new JTextPane();
		configurarTxtPane(txtpnPesoAgm, "Peso AGM");
		add(txtpnPesoAgm);
		
		txtpnValorAristaMenor = new JTextPane();
		configurarTxtPane(txtpnValorAristaMenor, "");
		add(txtpnValorAristaMenor);
		
		txtpnValorAristaMayor = new JTextPane();
		configurarTxtPane(txtpnValorAristaMayor, "");
		add(txtpnValorAristaMayor);
		
		txtpnValorPromedioAristas = new JTextPane();
		configurarTxtPane(txtpnValorPromedioAristas, "");
		add(txtpnValorPromedioAristas);
		
		txtpnValorPesoAGM = new JTextPane();
		configurarTxtPane(txtpnValorPesoAGM, "");
		add(txtpnValorPesoAGM);
	}

	/**
	 * Configura el formato del panel de texto 'panelTexto'.
	 * @param JTextPane p panel que sera configurado
	 * @param String texto, lo que se mostrara en el panel
	 */
	private void configurarTxtPane(JTextPane panelTexto, String texto) 
	{
		panelTexto.setText(texto);
		
		if (texto.equals(""))
			panelTexto.setFont(new Font("OCR A Extended", Font.PLAIN, 12));
		else
			panelTexto.setFont(new Font("OCR A Extended", Font.BOLD, 13));
		
		panelTexto.setEditable(false);
	}

	/**
	 * Configura la informacion del AGM en los correspondientes paneles de texto.
	 * @param CentroOperaciones hq el CentroOperaciones que contiene el AGM
	 */
	public void setInfoAGM(CentroOperaciones hq)
	{
		AGM agm = hq.getAgentes().getAGM();
		if(agm!=null)
		{
			txtpnValorAristaMenor.setText(String.format("%1$.3f", agm.getAristaMenor()) + " Km");
			txtpnValorAristaMayor.setText(String.format("%1$.3f", agm.getAristaMayor()) + " Km");
			txtpnValorPromedioAristas.setText(String.format("%1$.3f", agm.getPromedioAristas()) + " Km");
			txtpnValorPesoAGM.setText(String.format("%1$.3f", agm.getPesoAGM()) + " Km");
		}
		else
		{
			txtpnValorAristaMenor.setText("0.0 Km");
			txtpnValorAristaMayor.setText("0.0 Km");
			txtpnValorPromedioAristas.setText("0.0 Km");
			txtpnValorPesoAGM.setText("0.0 Km");
		}
	}
}
