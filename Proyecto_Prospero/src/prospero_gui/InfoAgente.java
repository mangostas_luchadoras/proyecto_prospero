package prospero_gui;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;
import prospero_negocio.Espia;
import java.awt.Rectangle;
import java.awt.Color;

public class InfoAgente extends JPanel 
{	
	private static final long serialVersionUID = 1L;
	private JLabel lblImagen;
	private JTextPane txtpnNombre;
	private JTextPane txtpnLatitud;
	private JTextPane txtpnLongitud;
	private JTextPane txtpnValorLatitud;
	private JTextPane txtpnValorLongitud;

	/**
	 * Crea el panel donde se muestra la informacion de los agentes.
	 */
	public InfoAgente() 
	{
		setLayout(null);
		setBackground(new Color(25, 25, 112));		
		setBounds(new Rectangle(0, 0, 896, 92));
		
		ImageIcon imagen = new ImageIcon(this.getClass().getResource("/image/mangosta_espia.png"));
		lblImagen = new JLabel();
		lblImagen.setBounds(5, 5, 90, 82);
		lblImagen.setIcon(imagen);
		add(lblImagen);
		
		txtpnNombre = new JTextPane();
		txtpnNombre.setBounds(170, 36, 207, 23);
		configurarTxtPane(txtpnNombre, "Nombre Clave");
		add(txtpnNombre);
		
		txtpnLatitud = new JTextPane();
		txtpnLatitud.setBounds(486, 12, 90, 21);
		configurarTxtPane(txtpnLatitud, "Latitud");
		add(txtpnLatitud);
		
		txtpnLongitud = new JTextPane();
		txtpnLongitud.setBounds(486, 59, 90, 21);
		configurarTxtPane(txtpnLongitud, "Logitud");
		add(txtpnLongitud);
		
		txtpnValorLatitud = new JTextPane();
		txtpnValorLatitud.setBounds(605, 12, 179, 21);
		configurarTxtPane(txtpnValorLatitud, "");
		add(txtpnValorLatitud);
		
		txtpnValorLongitud = new JTextPane();
		txtpnValorLongitud.setBounds(605, 59, 179, 21);
		configurarTxtPane(txtpnValorLongitud, "");
		add(txtpnValorLongitud);
	}

	/**
	 * Configura el formato del panel de texto 'panelTexto'.
	 * @param JTextPane p panel que sera configurado
	 * @param String texto, lo que se mostrara en el panel
	 */
	private void configurarTxtPane(JTextPane panelTexto, String texto) {
		panelTexto.setText(texto);
		
		if (texto.equals(""))
			panelTexto.setFont(new Font("OCR A Extended", Font.PLAIN, 14));
		else
			panelTexto.setFont(new Font("OCR A Extended", Font.BOLD, 16));
		
		panelTexto.setEditable(false);
	}
	
	/**
	 * Configura la informacion del Espia en los respectivos campos.
	 * En caso de que el Espia no sea valido, se muestran valores por defecto.
	 * @param Espia agente, espia del cual se mostran los datos
	 * @param boolean eliminandoEspia indica si se esta eliminando un Espia
	 */
	public void setInfoEspia(Espia agente, boolean eliminandoEspia)
	{
		if(agente!=null && !eliminandoEspia)
		{
			remove(lblImagen);
			remove(txtpnNombre);
			txtpnNombre.setText(agente.getMarker().getName());
			txtpnValorLatitud.setText(Double.toString(agente.getUbicacion().getLat()) + "°");
			txtpnValorLongitud.setText(Double.toString(agente.getUbicacion().getLon()) + "°");
			lblImagen.setIcon(agente.getFoto());
			add(lblImagen);
			add(txtpnNombre);
		}
		else
		{
			remove(lblImagen);
			remove(txtpnNombre);
			txtpnNombre.setText("Nombre Clave");
			txtpnValorLatitud.setText("");
			txtpnValorLongitud.setText("");
			lblImagen.setIcon(new ImageIcon(this.getClass().getResource("/image/mangosta_espia.png")));
			add(lblImagen);
			add(txtpnNombre);
		}
	}
}
