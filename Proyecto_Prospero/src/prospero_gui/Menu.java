package prospero_gui;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu extends Fondo 
{
	private static final long serialVersionUID = 1L;
	private JButton btnInicio;
	private JButton btnCargar;
	
	/**
	 * Crea un menu de la aplicacion con dos botones.
	 * Boton 'INICIAR' para empezar una nueva red de espinaje,
	 * Boton 'CARGAR' para cargar la ultima red de espionaje guardada.
	 * @param CentroOperaciones hq donde se va a representar el menU
	 * @param String rutaImagen
	 */
	public Menu(CentroOperaciones hq, String rutaImagen) 
	{
		super(rutaImagen);
		super.borrarFondo();
		
		btnInicio = new JButton("INICIAR");
		acomodarBoton(btnInicio);
		btnInicio.setBounds(200, 450, 200, 80);
		btnInicio.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				setVisible(false);
				hq.mostrarComponentes();
			}
		});
		add(btnInicio);
		
		btnCargar = new JButton("CARGAR RED");
		acomodarBoton(btnCargar);
		btnCargar.setBounds(500, 450, 200, 80);
		btnCargar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				hq.cargarRedEspionaje();
				hq.mostrarEspiasEnMapa();
				hq.crearRedComunicacion();
				setVisible(false);
				hq.mostrarComponentes();
			}
		});
		add(btnCargar);
			
		super.pintarFondo();
	}
	
	/**
	 * Configura las caracteristicas del boton.
	 * @param JButton boton
	 */
	private void acomodarBoton(JButton boton)
	{
		boton.setBorder(null);
		boton.setBorderPainted(false);
		boton.setContentAreaFilled(false);
		boton.setAlignmentX(Component.CENTER_ALIGNMENT);
		boton.setForeground(Color.WHITE);
		boton.setHorizontalTextPosition(SwingConstants.CENTER);
		boton.setIcon(new ImageIcon(Controles.class.getResource("/image/boton_azul.png")));
		boton.setPressedIcon(new ImageIcon(Controles.class.getResource("/image/boton_verde.png")));
		boton.setFont(new Font("OCR A Extended", Font.BOLD, 24));
	}
}
