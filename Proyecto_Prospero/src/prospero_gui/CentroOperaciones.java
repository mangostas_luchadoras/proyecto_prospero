package prospero_gui;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import prospero_negocio.AGM;
import prospero_negocio.Arista;
import prospero_negocio.Espia;
import prospero_negocio.RedEspionaje;

public class CentroOperaciones 
{
	private JFrame frame;
	private Fondo presentacion;
	private Menu menu;
	private RedEspionaje agentes;
	private JMapViewer mapa;
	private ArrayList<MapPolygonImpl> caminos;
	private Controles control;
	private InfoAgente datosAgente;
	private InfoAGM datosAGM;
	
	/**
	 * Activa la aplicacion.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					CentroOperaciones ventana = new CentroOperaciones();
					ventana.frame.setVisible(true);
					
					Timer timer = new Timer(2000, new ActionListener()
					{
					    @Override  
						public void actionPerformed(ActionEvent evt)
					    {
					    	ventana.presentacion.setVisible(false);
					    	ventana.menu.setVisible(true);
					    }
					});
					timer.setRepeats(false);		
					timer.start();
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Crea la aplicacion.
	 */
	public CentroOperaciones() 
	{
		initialize();
	}

	/**
	 * Inicializa el contenido del frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		presentacion = new Fondo("/image/mangosta_presentacion.png");
		menu = new Menu(this, "/image/menu_prospero.png");
		agentes = new RedEspionaje();
		mapa = new JMapViewer();
		caminos = new ArrayList<MapPolygonImpl>();
		control = new Controles(this);
		datosAGM = new InfoAGM();
		datosAgente = new InfoAgente();
		
		configurarComponentes();
		agregarComponentes();
		ocultarComponentes();
		getInfoDeEspia();
		datosAGM.setInfoAGM(this);
		ajustarVistaDelMapa();
	}
	
	/**
	 *Muestra en el mapa los marcadores que representan a los espias. 
	 */
	public void mostrarEspiasEnMapa() 
	{
		borrarEspiasEnMapa();
		for (Espia e : agentes) 
			mapa.addMapMarker(e.getMarker());
	}
	
	/**
	 * Quita del mapa los marcadores que representan a los espias.
	 */
	public void borrarEspiasEnMapa()
	{
		mapa.removeAllMapMarkers();
	}

	/**
	 * Crea los MapPolygon que representan las aristas del AGM.
	 */
	public void crearRedComunicacion() 
	{
		borrarRedComunicacion();
		caminos = new ArrayList<MapPolygonImpl>();
		AGM aristas = agentes.getAGM();
		if(aristas != null)
			for (Arista arista : aristas)
			{
				Coordinate origen = agentes.getEspia(arista.getVerticeU()).getUbicacion();
				Coordinate destino = agentes.getEspia(arista.getVerticeV()).getUbicacion();
				caminos.add(new MapPolygonImpl(origen, destino, origen));
			}	
		datosAGM.setInfoAGM(this);
	}
	
	/**
	 * Quita del mapa los MapPolygon que representan las aristas del AGM.
	 */
	private void borrarRedComunicacion()
	{
		mapa.removeAllMapPolygons();
	}
	
	/**
	 * Muestra en el mapa los MapPolygon que representan las aristas del AGM.
	 * @param boolean redVisible indica si esta presionado el boton Red Comunicacion
	 */
	public void mostrarRedComunicacion(boolean redVisible)
	{
		if(redVisible)
			for (MapPolygonImpl camino : caminos) 
				mapa.addMapPolygon(camino);
		else
			borrarRedComunicacion();
	}
	
	/**
	 * Devuelve la RedEspionaje que contiene a los espias.
	 * @return RedEspionaje agentes
	 */
	public RedEspionaje getAgentes()
	{
		return agentes;
	}
	
	/**
	 * Asigna a agentes la RedEspionaje que recibe.
	 * @param RedEspionaje red
	 */
	public void setAgentes(RedEspionaje red)
	{
		agentes = red;
	}
	
	/**
	 * Devuelve el JMapViewer sobre el que se trabaja.
	 * @return JMapViewer mapa
	 */
	public JMapViewer getMapa()
	{
		return mapa;
	}
	
	/**
	 * Muestra en pantalla la info del Espia seleccionado con el mouse desde el mapa.
	 */
	public void getInfoDeEspia()
	{
		mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent f) 
			{
				if (f.getButton() == MouseEvent.BUTTON1)
				{
					Espia agente = null;
					Coordinate posicion = (Coordinate)mapa.getPosition(f.getPoint());
					try
					{
						agente = agentes.getEspiaDesdeCoordenada(posicion);	
					}
				    catch (NullPointerException e)
					{ 
					}
					quitarComponentes();
					datosAgente.setInfoEspia(agente, control.eliminandoAgente());
					agregarComponentes();
					frame.getContentPane().repaint();
				}
			}
		});
	}
	
	/**
	 * Guarda la RedEspionaje actual en el archivo "red.save".
	 */
	public void guardarRedEspionaje() 
	{	
		try 
		{
			FileOutputStream fos = new FileOutputStream("red.save");
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(agentes);
			out.close();
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(frame, ex.getMessage());
		}
	}
	
	/**
	 * Lee la RedEspionaje guardada en el archivo "red.save".
	 */
	public void cargarRedEspionaje()
	{
		RedEspionaje red = null;
		try
		{
			FileInputStream fis = new FileInputStream("red.save");
			ObjectInputStream in = new ObjectInputStream(fis);
			red = (RedEspionaje) in.readObject();
			in.close();
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(frame, ex.getMessage());
			red = new RedEspionaje();
		}
		
		agentes = red;
		ajustarVistaDelMapa();
	}
	
	/**
	 * Agrega los distintos componentes de la ventana a esta.
	 */
	private void agregarComponentes()
	{
		frame.getContentPane().add(presentacion);
		frame.getContentPane().add(menu);
		mapa.add(control);
		frame.getContentPane().add(mapa);
		frame.getContentPane().add(datosAGM);
		frame.getContentPane().add(datosAgente);
	}
	
	/**
	 * Configura los valores y formato de los distintos componentes de la ventana.
	 */
	private void configurarComponentes() 
	{
		frame.setBounds(100, 100, 900, 700);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(CentroOperaciones.class.getResource("/image/mangosta_icon.jpeg")));
		frame.setTitle("Proyecto Prospero");
				
		control.setBorder(null);
		control.setBounds(736, 0, 160, 160);
		
		mapa.setBounds(0, 92, 896, 525);
		
		datosAGM.setBounds(0, 619, 896, 51);
		
		datosAgente.setBounds(0, 0, 896, 92);
	}
	
	/**
	 * Quita los componentes de la ventana.
	 */
	private void quitarComponentes()
	{
		frame.getContentPane().remove(datosAGM);
		frame.getContentPane().remove(datosAgente);
		frame.getContentPane().remove(mapa);
	}
	
	/**
	 * Oculta los componentes de la ventana.
	 */
	private void ocultarComponentes()
	{
		menu.setVisible(false);
		mapa.setVisible(false);
		control.setVisible(false);
		datosAGM.setVisible(false);
		datosAgente.setVisible(false);	
	}
	
	/**
	 * Vuelve visibles los componentes de la ventana.
	 */
	protected void mostrarComponentes()
	{
		datosAGM.setVisible(true);
		control.setVisible(true);
		datosAgente.setVisible(true);
		mapa.setVisible(true);
	}
	
	/**
	 * Ajusta la posicion y zoom del mapa de modo que puedan verse todos lo espias en este.
	 * En caso contrario inicia el mapa en una hubicacion prefijada con nivel de zoom 16.
	 */
	private void ajustarVistaDelMapa()
	{
		if(agentes.tamanioRedEspias()==0)
			mapa.setDisplayPosition(new Coordinate(-34.522222, -58.7), 16);
		else
		{	
			mostrarEspiasEnMapa();
			mapa.setDisplayToFitMapMarkers();
		}
	}
}
